package com.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JoinController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String url = "jdbc:mysql://localhost:3306/tugas16?user=oki&password=admin";
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            Connection con = DriverManager.getConnection(url);
            Statement mb = con.createStatement();
            ResultSet results = mb.executeQuery("SELECT mahasiswa.nama as nama_mahasiswa, pengajar.nama as nama_pengajar, mata_pelajaran.nama as nama_mapel, mahasiswa.nilai as nilai_mahasiswa, pengajar.jadwal FROM mata_pelajaran INNER JOIN mahasiswa ON mahasiswa.id_mata_pelajaran=mata_pelajaran.id INNER JOIN pengajar ON pengajar.id_mata_pelajaran=mata_pelajaran.id");
            req.setAttribute("val",results);
            RequestDispatcher request = req.getRequestDispatcher("TampilJoin.jsp");
            request.forward(req,resp);
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
