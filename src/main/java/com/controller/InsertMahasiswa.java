package com.controller;

import com.model.ModelDatabase;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class InsertMahasiswa extends HttpServlet {
    ModelDatabase mb = new ModelDatabase();
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ResultSet results = mb.get("mata_pelajaran");
        request.setAttribute("val",results);
        RequestDispatcher rs = request.getRequestDispatcher("FormMahasiswa.jsp");
        rs.forward(request,response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Map data = new HashMap();
        data.put("id_mata_pelajaran",Integer.parseInt(request.getParameter("id_mata_pelajaran")));
        data.put("nama",request.getParameter("nama"));
        data.put("jurusan",request.getParameter("jurusan"));
        data.put("semester",Integer.parseInt(request.getParameter("semester")));
        data.put("fakultas",request.getParameter("fakultas"));

        mb.insert("mahasiswa",data);
        response.sendRedirect("/tugas1/TampilMahasiswa");
    }
}
