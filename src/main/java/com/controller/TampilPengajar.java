package com.controller;

import com.model.ModelDatabase;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;

public class TampilPengajar extends HttpServlet {
    ModelDatabase mb = new ModelDatabase();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            ResultSet results = mb.get("pengajar");
            req.setAttribute("val",results);
            RequestDispatcher request = req.getRequestDispatcher("TampilPengajar.jsp");
            request.forward(req,resp);
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
