package com.controller;

import com.model.ModelDatabase;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class UpdateMataPelajaran  extends HttpServlet {
    ModelDatabase mb = new ModelDatabase();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResultSet results = mb.get("mata_pelajaran");
        req.setAttribute("val",results);
        // ambil data
        try{
            Map where = new HashMap();
            where.put("id", req.getParameter("id"));
            ResultSet res = mb.get_where("mata_pelajaran", where);
            req.setAttribute("data", res);
        }catch (Exception e){
            e.printStackTrace();
        }
        RequestDispatcher res = req.getRequestDispatcher("FormMapelEdit.jsp");
//        mengembalikan nilai request dan response
        res.forward(req,resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
Map where = new HashMap();

        where.put("id" ,req.getParameter("id"));
        Map data = new HashMap();
        data.put("nama",req.getParameter("namaMataPelajaran"));
        mb.update("mata_pelajaran", data, where);
        resp.sendRedirect("/tugas1/TampilMataPelajaran");


    }
}
