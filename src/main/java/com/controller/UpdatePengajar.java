package com.controller;

import com.model.ModelDatabase;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class UpdatePengajar extends HttpServlet {
    ModelDatabase mb = new ModelDatabase();
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResultSet results = mb.get("mata_pelajaran");
        req.setAttribute("val",results);
        // ambil data
        try{
            Map where = new HashMap();
            where.put("id", req.getParameter("id"));
            ResultSet res = mb.get_where("pengajar", where);
            req.setAttribute("data", res);
        }catch (Exception e){
            e.printStackTrace();
        }
        RequestDispatcher res = req.getRequestDispatcher("FormEditPengajar.jsp");
//        mengembalikan nilai request dan response
        res.forward(req,resp);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map where = new HashMap();
        where.put("id",Integer.parseInt(request.getParameter("id")));

        Map data = new HashMap();
        data.put("id_mata_pelajaran",request.getParameter("id_mata_pelajaran"));
        data.put("nama",request.getParameter("nama"));
        data.put("jurusan",request.getParameter("jurusan"));
        data.put("semester",Integer.parseInt(request.getParameter("semester")));
        data.put("fakultas",request.getParameter("fakultas"));

        mb.update("mahasiswa",data,where);
        response.sendRedirect("/tugas1/TampilMahasiswa");
    }
}
