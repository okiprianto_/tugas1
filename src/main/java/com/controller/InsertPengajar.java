package com.controller;

import com.model.ModelDatabase;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class InsertPengajar extends HttpServlet {
    ModelDatabase mb = new ModelDatabase();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResultSet results = mb.get("mata_pelajaran");
        req.setAttribute("val",results);
        RequestDispatcher rs = req.getRequestDispatcher("FormPengajar.jsp");
        rs.forward(req,resp);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Map data = new HashMap();
        data.put("id_mata_pelajaran",Integer.parseInt(request.getParameter("id_mata_pelajaran")));
        data.put("nama",request.getParameter("nama"));
        data.put("jadwal",request.getParameter("jadwal"));
        data.put("tahun_bergabung",Integer.parseInt(request.getParameter("tahun_bergabung")));
        data.put("alamat", request.getParameter("alamat"));

        mb.insert("pengajar",data);
        response.sendRedirect("/tugas1/TampilPengajar");
    }
}
