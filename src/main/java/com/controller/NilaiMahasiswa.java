package com.controller;

import com.model.ModelDatabase;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class NilaiMahasiswa extends HttpServlet {
    ModelDatabase mb = new ModelDatabase();
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            Map where = new HashMap();
            where.put("id_mata_pelajaran", req.getParameter("id"));
            ResultSet res = mb.get_where("mahasiswa", where);
            req.setAttribute("data", res);
            req.setAttribute("id", req.getParameter("id"));
        }catch (Exception e){
            e.printStackTrace();
        }
        RequestDispatcher res = req.getRequestDispatcher("FormNilaiMahasiswa.jsp");
//        mengembalikan nilai request dan response
        res.forward(req,resp);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // update in tabel mahasiswa
        Map where = new HashMap();
        where.put("id",Integer.parseInt(request.getParameter("id_mahasiswa")));
        Map data = new HashMap();
        data.put("nilai",Integer.parseInt(request.getParameter("nilai")));
        mb.update("mahasiswa",data,where);

        //insert tabel join
        Map data2 = new HashMap();
        data2.put("id_mahasiswa", Integer.parseInt(request.getParameter("id_mahasiswa")));
        data2.put("id_matapelajaran", Integer.parseInt(request.getParameter("id_mapel")));
        data2.put("id_pengajar", Integer.parseInt(request.getParameter("id_mapel")));
        mb.insert("mengajar",data2);

        response.sendRedirect("/tugas1/TampilPengajar");
    }
}
