package com.controller;

import com.model.ModelDatabase;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class InsertMataPelajaran extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
// ambil data
        RequestDispatcher res = req.getRequestDispatcher("FormMapel.jsp");
//        mengembalikan nilai request dan response
        res.forward(req,resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Map data = new HashMap();
        data.put("id" ,req.getParameter("id"));
        data.put("nama",req.getParameter("namaMataPelajaran"));


        ModelDatabase model = new ModelDatabase();
        model.insert("mata_pelajaran", data);
        resp.sendRedirect("/tugas1/TampilMataPelajaran");


}
}
