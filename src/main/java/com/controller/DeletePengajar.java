package com.controller;

import com.model.ModelDatabase;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DeletePengajar extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ModelDatabase mb = new ModelDatabase();

        Map where = new HashMap();
        where.put("id",Integer.parseInt(request.getParameter("id")));

        mb.delete("pengajar",where);
        response.sendRedirect("/tugas1/TampilPengajar");
    }
}
