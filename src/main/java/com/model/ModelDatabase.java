package com.model;

import java.sql.*;
import java.util.Map;

public class ModelDatabase {
    public Connection conn = getConnection();

    public Connection getConnection(){
        try {
            String url = "jdbc:mysql://localhost:3306/tugas16?user=oki&password=admin";
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            this.conn = DriverManager.getConnection(url);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return this.conn;
    }

    // Proses ambil semua data pada tabel
    public ResultSet get(String table) {
        try {
            /*
             * Pada method get memiliki parameter yaitu nama_tabel yang akan di ambil datanya, setelah itu buatlah sebuah
             * statment dengan nama stmt dan isinya variabel conn.createStatement, dan buat lah ReseltSet dengan nama result
             * dari stmt.executeQuery(query), setelah itu balikan nilai resultnya
             * */

            Statement stmt = this.conn.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * FROM " + table);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Proses ambil data sesuai dengan data yang di cari
    public ResultSet get_where(String table, Map where) {
        /*
         * Pada method get_where memiliki parameter yaitu nama_tabel dan where(menerima hasil map karena untuk mengambil
         * key dan value yang di perlukan oleh method), setelah itu lakukan perulangan dengan mengambil key valuenya
         * terlebih dahulu setelah itu buatlah sebuah statment dengan nama stmt dan isinya variabel conn.createStatement,
         * dan buat lah ReseltSet dengan nama result dari stmt.executeQuery(query), setelah itu balikan nilai resultnya
         * */

        // variabel untuk menampung where yang di inputkan
        String whereString = "";

        for (Object key : where.keySet())
        {
            whereString += key + "='"+ where.get(key) +"' AND ";// agar mendapatkan hasil nama='oki' AND
        }

        //lakukan pemotongan panjang string yaitu 5 paling belakang agar di akhir string tidak ada tulisan AND
        whereString = whereString.substring(0, whereString.length() - 5);

        try {
            Statement stmt = this.conn.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * FROM " + table + " WHERE " + whereString);

            return result;

        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Proses input data kepada tabel
    public void insert(String table, Map data) {
        /*
         * Pada method insert memiliki parameter yaitu nama_tabel dan data(menerima hasil map karena untuk mengambil
         * key dan value yang di perlukan oleh method), setelah itu lakukan perulangan dengan mengambil key valuenya
         * terlebih dahulu setelah itu buatlah sebuah statment dengan nama stmt dan isinya variabel conn.createStatement,
         * dan buat lah ReseltSet dengan nama result dari stmt.executeUpdate(query), setelah itu balikan nilai resultnya
         * */

        // variabel untuk menampung nama field tabel dan hasil insert
        String fields = "";
        String insert = "";

        for(Object key : data.keySet())
        {
            fields += key + ",";// agar mendapatkan hasil id,nama,
            insert += "'"+ data.get(key) +"',";// agar mendapatkan hasil '1','oki',
        }

        // untuk mehilangkan koma di akhir string field dan insert
        fields = fields.substring(0, fields.length() - 1);
        insert = insert.substring(0, insert.length() - 1);

        try {
            Statement stmt = this.conn.createStatement();

            stmt.executeUpdate("INSERT INTO " + table + "("+ fields +") VALUES("+ insert +")");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // Proses merubah data tabel
    public void update(String table, Map data, Map where) {
        /*
         * Pada method update memiliki parameter yaitu nama_tabel,where dan data(menerima hasil map karena untuk mengambil
         * key dan value yang di perlukan oleh method), setelah itu lakukan perulangan dengan mengambil key valuenya
         * terlebih dahulu setelah itu buatlah sebuah statment dengan nama stmt dan isinya variabel conn.createStatement,
         * dan buat lah ReseltSet dengan nama result dari stmt.executeUpdate(query), setelah itu balikan nilai resultnya
         * */

        // variabel untuk menampung string setString dan whereString
        String setString = "";
        String whereString = "";

        for(Object key : data.keySet())
        {
            setString += key + "='"+ data.get(key) +"',"; //agar mendapatkan hasil nama='oki',
        }

        for(Object key : where.keySet())
        {
            whereString += key + "='"+ where.get(key) +"' AND ";//agar mendapatkan hasil id='1',
        }

        // untuk mehilangkan koma di akhir string setString dan whereString
        setString = setString.substring(0, setString.length() - 1);
        whereString = whereString.substring(0, whereString.length() - 5);

        try {
            Statement stmt = this.conn.createStatement();

            stmt.executeUpdate("UPDATE " + table + " SET " + setString + " WHERE " + whereString);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    // Proses menghapus data tabel berdasarkan id yang di cari atau dengan field yg unique
    public void delete(String table, Map where) {
        /*
         * Pada method delete memiliki parameter yaitu nama_tabel,where(menerima hasil map karena untuk mengambil
         * key dan value yang di perlukan oleh method), setelah itu lakukan perulangan dengan mengambil key valuenya
         * terlebih dahulu setelah itu buatlah sebuah statment dengan nama stmt dan isinya variabel conn.createStatement,
         * dan buat lah ReseltSet dengan nama result dari stmt.executeUpdate(query), setelah itu balikan nilai resultnya
         * */

        //variabel untuk menampung whereString
        String whereString = "";

        for(Object key : where.keySet())
        {
            whereString += key + "='"+ where.get(key) +"' AND ";//agar mendapatkan hasil id='1' AND
        }

        // untuk mehilangkan AND di akhir string whereString
        whereString = whereString.substring(0, whereString.length() - 5);

        try {
            Statement stmt = this.conn.createStatement();

            stmt.executeUpdate("DELETE FROM " + table + " WHERE " + whereString);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
