<%@ page import="java.sql.*"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
      crossorigin="anonymous"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
    />

    <title>Document</title>
  </head>
  <body>
    <h2 class="card-text text-center mt-4">Data Pengajar</h2>
    <hr style="width: 20%" class="mb-4" />
    <div class="container" style="padding-left:110px">
        <div class="text-center mb-4 btn btn-group">
             <a href="Menu" class="btn btn-outline-secondary">
               <i class="fas fa-arrow-left"></i> Kembali
             </a>
             <a href="InsertPengajar" class="btn btn-primary">
              <i class="fas fa-plus"></i> Tambah
             </a>
        </div>
    </div>
    <table class="table table-bordered" style="width: 70%; margin: 0 auto; text-align:center">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col" style="width:20%">Nama</th>
          <th scope="col">ID Mata Pelajaran</th>
          <th scope="col">Tahun Bergabung</th>
          <th scope="col" style="width:20%">Jadwal</th>
          <th scope="col" style="width:20%">Alamat</th>
          <th scope="col" style="width:30%">Setting</th>
        </tr>
      </thead>
      <tbody>
       <% ResultSet r = (ResultSet) request.getAttribute("val");
            int i =1;
       %>
       <%while(r.next()){%>
        <tr>
          <th scope="row"><%=i++%></th>
          <td><%=r.getString("nama")%></td>
          <td><%=r.getString("id_mata_pelajaran")%></td>
          <td><%=r.getString("tahun_bergabung")%></td>
          <td><%=r.getString("jadwal")%></td>
          <td><%=r.getString("alamat")%></td>
          <td>
            <div class="btn btn-group">
                <a href="NilaiMahasiswa?id=<%=r.getString("id")%>" class="btn btn-success">
                  <i class="fas fa-paper"></i>TambahNilai
                </a>
                <form method="post" action="DeletePengajar?id=<%=r.getString("id")%>">
                    <button class="btn btn-danger" type="submit" onclick="return confirm('apakah anda yakin ingin menghapus data ini?')">
                        <i class="fas fa-trash"></i>delete
                    </button
                </form>
            </div>
          </td>
        </tr>
        <%}%>
      </tbody>
    </table>

    <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
      integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
      integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
      crossorigin="anonymous"
    ></script>
  </body>
</html>
