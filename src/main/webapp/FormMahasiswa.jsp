<%@ page import="java.sql.*"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
      crossorigin="anonymous"
    />
  </head>
  <body>
    <h2 class="card-text text-center my-4">Form Mahasiswa</h2>
    <hr style="width: 20%" />
    <div class="row">
      <div class="col-sm-4">
        <form action="InsertMahasiswa" method="post" class="ml-4">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan nama" />

          <% ResultSet r = (ResultSet) request.getAttribute("val"); %>
          <label for="idMataPelajaran">ID Mata Pelajaran</label>
          <select class="form-control" name="id_mata_pelajaran" id="id_mata_pelajaran">
            <%while(r.next()){%>
            <option value="<%=r.getString("id")%>"><%=r.getString("nama")%></option>
            <%}%>
          </select>

          <label for="jurusan">Jurusan</label>
          <input type="text" name="jurusan" class="form-control" id="jurusan" placeholder="jurusan"/>

          <label for="semester">Semester</label>
          <input type="number" placeholder="semester" name="semester" class="form-control" />

          <label for="fakultas">Fakultas</label>
          <input type="text" class="form-control" name="fakultas" placeholder="fakultas" id="fakultas"/>

          <button type="submit" class="btn btn-success my-4">Simpan</button>
          <a href="TampilMahasiswa" class="btn btn-outline-secondary">Kembali</a>
        </form>
      </div>
    </div>

    <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
      integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
      integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
      crossorigin="anonymous"
    ></script>
  </body>
</html>
