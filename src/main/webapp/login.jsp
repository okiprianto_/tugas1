<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
      crossorigin="anonymous"
    />

    <style type="text/css">
      table {
        padding: 50px;
        margin-top:5%;
      }

      table .imageLogin {
        width: 50em;
        height: 30em;
      }

      .formLogin {
        width: 100%;
      }
    </style>

    <title>Document</title>
  </head>
  <body>
    <div class="tabelLogin">
      <table border="0" width="80%">
        <tr>
          <td>
            <img
              src="login2.png"
              alt="gambarLogin"
              class="img-fluid imageLogin"
            />
          </td>
          <td>
            <div class="formLogin">
              <h2>Login</h2>
              <hr />
              <form action="LoginController" method="post">

              <input
                type="text"
                class="form-control"
                name="username"
                placeholder="username"
              /><br />
              <input
                type="password"
                name="password"
                class="form-control"
                placeholder="password"
              /><br />
              <button type="submit" class="btn btn-info">Login</button>

              </form>
            </div>
          </td>
        </tr>
      </table>
    </div>

    <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
      integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
      integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
      crossorigin="anonymous"
    ></script>
  </body>
</html>
