<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

<style type="text/css">

*{
padding : 0;
margin:0;
}

body{
overflow-x : hidden;
}


.navbar,.cover,footer {
background: linear-gradient(90deg, rgba(44,179,167,1) 0%, rgba(42,184,151,1) 36%, rgba(20,224,149,1) 100%);

}

.underline {
  height: 5px;
  width: 150px;
  margin: 0 auto;
}

</style>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark">
      <a class="navbar-brand" href="#">Myweb</a>
      <button
        class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#"
              >Home <span class="sr-only">(current)</span></a>
        </ul>
      </div>
    </nav>

    <section class="cover">
    <div class="row">
    <div class="col-sm-12 text-center">
     <h1 class="card-text text-white">Welcome ${requestScope['user'].username}, Have a Good Day</h1>
        <p class="card-text text-white">
                     Lorem ipsum dolor sit amet consectetur, adipisicing elit. Autem
                     saepe voluptatem porro voluptates voluptas officiis eos explicabo.
        </p>
            <img class="img-fluid" src="wave.png">
    </div>
    </div>
     </section>

<section class="service mt-3 px-3">
<h2 class="card-text text-center">service</h2>
<hr class="underline mb-4">
<div class="row justify-content-center">


<div class="col">
<div class="card bg-light mb-3" ">
  <div class="card-header bg-info text-white"> Tampil Mahasiswa</div>
  <div class="card-body">
    <h5 class="card-title">Tampil Data Mahasiswa</h5>
    <p class="card-text">klik tombol di bawah untuk melihat tabel Data Mahasiswa </p>
    <a href="TampilMahasiswa" class="btn btn-info">Tampil Data Mahasiswa </a>
  </div>
</div>
</div>

<div class="col">
<div class="card bg-light mb-3">
  <div class="card-header bg-info text-white">Tampil Data Pengajar</div>
  <div class="card-body">
    <h5 class="card-title">Tampil Data Pengajar</h5>
        <p class="card-text">klik tombol di bawah untuk melihat data pengajar </p>
        <a href="TampilPengajar" class="btn btn-info">Tampil Data Pengajar </a>
  </div>
</div>
</div>

<div class="col">
<div class="card bg-light mb-3">
  <div class="card-header bg-info text-white">Tampil Mata Pelajaran</div>
  <div class="card-body">
    <h5 class="card-title">Tampil Mapel</h5>
        <p class="card-text">klik tombol di bawah untuk melihat Tabel Mata pelajaran</p>
        <a href="TampilMataPelajaran" class="btn btn-info">Tampil Mapel</a>
  </div>
</div>
</div>

<div class="col">
<div class="card bg-light mb-3">
  <div class="card-header bg-info text-white">Tampil semua Data</div>
  <div class="card-body">
    <h5 class="card-title">Tampil Semua Data </h5>
        <p class="card-text">klik tombol di bawah untuk ke form Hapus data</p>
        <a href="TampilJoin" class="btn btn-info">Kelola Semua Tabel </a>
  </div>
</div>
</div>

</div>
</section>
<footer class="py-3 text-white mt-4">
      <p class="text-center">
        Copyright &copy; 2020 build by Oki & Fadlan
      </p>
    </footer>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

</body>
</html>

